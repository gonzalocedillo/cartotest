const express = require('express');
const app = express();
const PORT = process.env.PORT || 8888;

/**
* Dynamic route, in order to recycle functionality with multiple layer styles
*/
app.get('/:part/:x/:y/:z', (req, res) => {
  const fs = require('fs');
  const mapnik = require('mapnik');
  const path = require('path');

  // Tile's size
  const TILE_SIZE = 256;
  // Dynamic path for XML file
  const adminPath = `./style-${req.params.part}.xml`;
  // url params INT parsed values
  const xVal = parseInt(req.params.x);
  const yVal = parseInt(req.params.y);
  const zVal = parseInt(req.params.z);

  mapnik.register_datasource(path.join(mapnik.settings.paths.input_plugins, 'shape.input'));

  // New map object with constant size
  const map = new mapnik.Map(TILE_SIZE, TILE_SIZE);
  // Load shape, depending on XML solicited
  map.load(adminPath, function(err, map) {
    if (err) throw error;
    ///////
    // this method is not documented on Mapnik site. Apparently it doesn't affect
    // map.zoomAll();
    //////

    // new Vector based on params received
    const vt = new mapnik.VectorTile(xVal, yVal, zVal);

    // render a VectorTile on Map
    map.render(vt, function(err, tile) {
      if (err) throw error;
      // new image to set the Vector
      const im = new mapnik.Image(TILE_SIZE, TILE_SIZE);
      // render the tile into an image
      tile.render(map, im, (err, img) => {
        if (err) throw error;
        // encode generate image to get a buffer
        img.encode('png', function(err, buffer) {
          if (err) throw error;
          // resolve the request with the image's buffer
          res.send(buffer);
        });
      })

    });
  });
});

app.listen(PORT, () => console.log('Running server!'));
